package com.training;

public class StudentDetails {

    private final int rollNO;
    private final String name;
    private final int maths_marks;
    private final int CS_marks;
    private final double PCM_marks;
    private final int chemistry_marks;
    private final int physics_marks;
    public String group=null;
    public int rank=0;
    public String getName(){
        return this.name;
    }

    public StudentDetails(int rollNo, String name, int CS_marks, int physics_marks,int chemistry_marks, int maths_marks) {
        this.rollNO=rollNo;
        this.name=name;
        this.maths_marks=maths_marks;
        this.CS_marks=CS_marks;
        this.physics_marks=physics_marks;
        this.chemistry_marks=chemistry_marks;
        this.PCM_marks=(double)(physics_marks+chemistry_marks+maths_marks)/3;
    }
    public int getMaths_marks(){
        return maths_marks;
    }

    public int getCS_marks() {
        return CS_marks;
    }

    public double getPCM_marks() {
        return PCM_marks;
    }
    public int getTotalMarks(){
        return physics_marks+chemistry_marks+maths_marks+CS_marks;
    }
}
