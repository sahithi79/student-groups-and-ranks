package com.training;

import java.util.*;

public class ClassTeacher {
    public static void main(String[] args) {
        int n;
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter Number of Students in Class:");
        n = scan.nextInt();
        System.out.println("Enter Students Details:");
        StudentDetails[] s = new StudentDetails[n];
        HashMap<String, Integer> list = new HashMap<>();
        list = divideGroups(list, n, s);
        printGroups(s,n);
        Map<String, Integer> listOfStudents = sortByValue(list);
        printStudentsDetails(listOfStudents,s);
    }

    private static void printGroups(StudentDetails[] s, int n) {
        for(int i=0;i<n;i++){
            System.out.println(s[i].getName() +" is assigned "+s[i].group+" group");
        }
        System.out.println();
    }

    private static HashMap<String, Integer> divideGroups(HashMap<String, Integer> list, int n, StudentDetails[] s) {
        Scanner scan = new Scanner(System.in);
        for (int i = 0; i < n; i++) {
            System.out.println("Enter Name of the student");
            String name = scan.next();
            System.out.println("Enter marks in CS,Physics,Chemistry,Mathematics:");
            int[] marks = new int[4];
            for (int j = 0; j < 4; j++) {
                marks[j] = scan.nextInt();
            }

            s[i] = new StudentDetails(i + 1, name, marks[0], marks[1], marks[2],marks[3]);
            if (s[i].getCS_marks() > 80 && s[i].getPCM_marks() > 70) {
                s[i].group = "CS";
            } else if (s[i].getPCM_marks() > 70) {
                s[i].group = "Biology";
            } else if (s[i].getMaths_marks() > 80) {
                s[i].group = "Commerce";
            } else {
                s[i].group = "no";
            }
            list.put(s[i].getName(), s[i].getTotalMarks());

        }
        return list;
    }

    public static HashMap<String, Integer> sortByValue(HashMap<String, Integer> hm)
    {
        List<Map.Entry<String, Integer> > list = new LinkedList<Map.Entry<String, Integer> >(hm.entrySet());

        Collections.sort(list, new Comparator<Map.Entry<String, Integer> >() {
            public int compare(Map.Entry<String, Integer> o1,
                               Map.Entry<String, Integer> o2)
            {
                return (o2.getValue()).compareTo(o1.getValue());
            }
        });

        HashMap<String, Integer> temp = new LinkedHashMap<String, Integer>();
        for (Map.Entry<String, Integer> aa : list) {
            temp.put(aa.getKey(), aa.getValue());
        }
        return temp;
    }

    private static void printStudentsDetails(Map<String, Integer> listOfStudents, StudentDetails[] s) {
        int i=1;
        for(Map.Entry<String,Integer> en : listOfStudents.entrySet()){
            System.out.println("Rank"+i+":"+en.getKey()+" with total marks of "+en.getValue());
            i++;
        }
    }
}